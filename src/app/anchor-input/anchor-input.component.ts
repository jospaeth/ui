import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Anchor } from '../anchor';

@Component({
  selector: 'app-anchor-input',
  templateUrl: './anchor-input.component.html',
  styleUrls: ['./anchor-input.component.css']
})
export class AnchorInputComponent implements OnInit {
  
  @Output() anchorOutput: EventEmitter<Anchor> = new EventEmitter<Anchor>();

  anchor: Anchor;
  anchorString: string;
  
  alertAnchorParse: boolean = false;

  anchorStringInputEnabled: boolean = false;

  scannerEnabled: boolean = false;
  scanSuccess: boolean = false;
  scannedString: string = '';

  constructor() { }

  ngOnInit(): void {
    this.anchor = new Anchor('plaintext', '', '')
  }

  parseAnchorString(str: string): void {
    if (str !== undefined)
      var split = str.split(':', 3);
    if (str === undefined || split.length != 3) {
      this.alertAnchorParse = true;
      this.anchor.method = '';
      this.anchor.name = '';
      this.anchor.anchorData = '';
    } else {
      this.alertAnchorParse = false;
      this.anchor.method = split[0];
      this.anchor.name = split[1];
      this.anchor.anchorData = split[2];
      this.anchorOutput.emit(this.anchor);
    }
  }

  scanSuccessHandler(s: string): void {
    console.log('scanned: ' + s);
    this.alertAnchorParse = false;
    this.scannerEnabled = false;
    this.anchorStringInputEnabled = false;
    this.scanSuccess = true;
    this.scannedString = s;
    this.parseAnchorString(s);
  }

}
