import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Anchor } from '../anchor';
import { EscrowService } from '../escrow.service';

@Component({
  selector: 'app-import-identity',
  templateUrl: './import-identity.component.html',
  styleUrls: ['./import-identity.component.css']
})
export class ImportIdentityComponent implements OnInit {

  anchor: Anchor = new Anchor('plaintext', '', '');

  alertMethod: boolean = false;
  alertIdentityName: boolean = false;
  alertAnchorData: boolean = false;
  alertRestoreFailed: boolean = false;

  constructor(
    private escrowService: EscrowService,
    private router: Router
  ) { }

  ngOnInit(): void { }

  restoreEscrow(): void {
    if (!this.validateAnchor())
      return;
    this.escrowService.getIdentityFromEscrow(this.anchor).subscribe(
      id => {
        if (id === null) {
          console.warn('failed to restore identity!');
          this.alertRestoreFailed = true;
        } else {
          console.log('restored identity: ' + id.name);
          this.alertRestoreFailed = false;
          this.router.navigate(['/']);
        }
      }
    );
  }

  validateAnchor(): boolean {
    this.alertMethod = (this.anchor.method === '');
    this.alertIdentityName = (this.anchor.name === '');
    this.alertAnchorData = (this.anchor.anchorData === '');

    return !this.alertMethod && !this.alertIdentityName && !this.alertAnchorData;
  }

  getAnchor(anchor: Anchor): void {
    this.anchor = anchor;
  }

}
