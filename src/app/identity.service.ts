import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from './config.service';
import { Identity } from './identity';
import { IdentityWithPrivkey } from './identity-with-privkey';
import { GnuNetResponse } from './gnu-net-response';

@Injectable()
export class IdentityService {
  constructor(private http: HttpClient, private config: ConfigService) { }

  getIdentities(): Observable<Identity[]> {
    return this.http.get<any[]>(this.config.get().apiUrl + '/identity');
  }

  getIdentityFromPubkey(pubkey: string): Observable<Identity> {
    return this.http.get<any>(this.config.get().apiUrl + '/identity/pubkey/' + pubkey);
  }

  getIdentityFromName(name: string): Observable<Identity> {
    return this.http.get<any>(this.config.get().apiUrl + '/identity/name/' + name);
  }

  getIdentityWithPrivkeyFromName(name: string): Observable<IdentityWithPrivkey> {
    return this.http.get<any>(this.config.get().apiUrl + '/identity/name/' + name + '?private');
  }

  getIdentitiesWithPrivkey(): Observable<IdentityWithPrivkey[]> {
    return this.http.get<any[]>(this.config.get().apiUrl + '/identity?private');
  }

  getIdentityWithPrivkey(identityId: string): Observable<IdentityWithPrivkey> {
    return this.http.get<any>(this.config.get().apiUrl + '/identity/pubkey/' + identityId + '?private');
  }

  addIdentity(identity: Identity) {
    const obj = { 'name': identity.name };
    return this.http.post(this.config.get().apiUrl + '/identity/', obj);
  }

  deleteIdentity(identityId: string) {
    return this.http.delete(this.config.get().apiUrl + '/identity/pubkey/' + identityId);
  }

  importIdentity(identity: IdentityWithPrivkey) {
    const obj = { 'name': identity.name, 'privkey': identity.privkey };
    return this.http.post(this.config.get().apiUrl + '/identity/', obj);
  }
}
