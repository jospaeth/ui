import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IdentityService } from '../identity.service';
import { Identity } from '../identity';
import { EscrowService } from '../escrow.service';
import { Anchor } from '../anchor';
import { ClipboardService } from 'ngx-clipboard';
import { EscrowStatus } from '../escrow-status';

@Component({
  selector: 'app-escrow-identity',
  templateUrl: './escrow-identity.component.html',
  styleUrls: ['./escrow-identity.component.css']
})
export class EscrowIdentityComponent implements OnInit {

  identity: Identity = new Identity('', '');
  href: string;
  method: string;
  userSecret: string = '';

  anchor: Anchor = null;
  verifyAnchor = new Anchor('plaintext', '', '');
  anchorString: string;

  status: EscrowStatus = null;
  escrowStatusString: string = 'unknown';

  alertUserSecret: boolean = false;
  alertMethod: boolean = false;
  alertIdentityName: boolean = false;
  alertAnchorData: boolean = false;
  alertEscrowValid: boolean = false;
  alertEscrowInvalid: boolean = false;
  alertEscrowSharesMissing: boolean = false;

  constructor(
    private identityService: IdentityService,
    private escrowService: EscrowService,
    private clipboardService: ClipboardService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(p => {
      if (p['id'] === undefined) {
        return;
      }
      this.identityService.getIdentityFromName(p['id']).subscribe(
        id => {
          this.identity = id;
          this.updateStatus();
        });
    });
  }

  static anchorToString(anchor: Anchor): string {
    return anchor.method + ':' + anchor.name + ':' + anchor.anchorData;
  }

  updateStatus(): void {
    this.escrowService.getEscrowStatus(this.method, this.identity).subscribe(
      status => {
        this.status = status;
        this.updateStatusString();
      }
    );
  }

  updateStatusString(): void {
    if (this.status.lastMethod === 'none')
      this.escrowStatusString = 'not escrowed';
    else {
      if (new Date().getTime() >= new Date(this.status.nextRecommendedVerification - 1).getTime())
        this.escrowStatusString = 'unknown';
      else
        this.escrowStatusString = 'escrowed';
    }
  }

  methodRequiresUserSecret(method: string): boolean {
    var ms: string[] = ['gns'];
    return ms.includes(method);
  }

  startEscrow(): void {
    if (this.userSecret === '' && this.methodRequiresUserSecret(this.method)) {
      this.alertUserSecret = true;
      this.anchor = null;
      this.anchorString = '';
      return;
    } else {
      this.alertUserSecret = false;
    }
    this.escrowService.putIdentityInEscrow(this.method, this.identity, this.userSecret).subscribe(
      anchor => {
        this.anchor = anchor;
        this.anchorString = EscrowIdentityComponent.anchorToString(anchor);
        this.updateStatus();
      }
    );
  }

  updateQRImage(): boolean {
    var qrcode = document.getElementsByClassName('qrcode-svg');
    // TODO: maybe find a better way to do this!
    if (qrcode.length !== 0) {
      var svg1 = qrcode[0].getElementsByTagName('svg');
      if (svg1.length !== 0) {
        svg1[0].setAttribute('style', 'max-width: 100%;');
        var svg2 = svg1[0].getElementsByTagName('svg');
        svg2[0].setAttribute('width', '100%');
      }
    }
    return true;
  }

  isEscrowFinished(): boolean {
    return this.anchor !== null;
  }

  copyAnchorToClipboard(): void {
    this.clipboardService.copyFromContent(this.anchorString);
  }

  downloadQR(): void {
    this.href = document.getElementsByClassName('qrcode-img')[0]
      .getElementsByTagName('img')[0].src;
  }

  getStatusSymbol(): string {
    switch (this.escrowStatusString) {
      case 'escrowed':
        return 'check-circle';
      case 'not escrowed':
        return 'times-circle';
      default:
        return 'question-circle';
    }
  }

  getStatusColor(): string {
    switch (this.escrowStatusString) {
      case 'escrowed':
        return '#28a745';
      case 'not escrowed':
        return '#dc3545';
      default:
        return '#ffc107';
    }
  }

  verifyEscrow(): void {
    if (!this.validateVerifyAnchor())
      return;
    this.alertEscrowValid = false;
    this.alertEscrowInvalid = false;
    this.alertEscrowSharesMissing = false;
    this.escrowService.verifyEscrow(this.verifyAnchor.method, this.identity, this.verifyAnchor).subscribe(
      r => {
        switch (r.verificationResult) {
          case 'valid':
            this.alertEscrowValid = true;
            break;
          case 'invalid':
            this.alertEscrowInvalid = true;
            break;
          case 'shares_missing':
            this.alertEscrowSharesMissing = true;
            break;
          default:
            console.log('unknown verification result: ' + r)
        }
        this.updateStatus();
      }
    );
  }

  validateVerifyAnchor(): boolean {
    this.alertMethod = (this.verifyAnchor.method === '');
    this.alertIdentityName = (this.verifyAnchor.name === '');
    this.alertAnchorData = (this.verifyAnchor.anchorData === '');

    return !this.alertMethod && !this.alertIdentityName && !this.alertAnchorData;
  }

  getAnchor(anchor: Anchor): void {
    this.verifyAnchor = anchor;
  }

}
