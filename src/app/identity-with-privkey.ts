export class IdentityWithPrivkey {
    constructor(public pubkey: string,
        public privkey: string,
        public name: string) { }
}
