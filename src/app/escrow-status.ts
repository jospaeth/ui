export class EscrowStatus {
    constructor(
        public lastMethod: string,
        public lastSuccessfulVerification: number,
        public nextRecommendedVerification: number
    ) { }
}
