import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ConfigService } from './config.service';
import { Identity } from './identity';
import { Anchor } from './anchor';
import { EscrowStatus } from './escrow-status';
import { VerificationResult } from './verification-result';

@Injectable()
export class EscrowService {
  constructor(private http: HttpClient, private config: ConfigService) { }

  static methodAppendix(method: string): string {
    return method !== undefined ? '?method=' + method : '';
  }

  putIdentityInEscrow(method: string, identity: Identity, userSecret: string): Observable<Anchor> {
    var json = {
      "userSecret": userSecret
    }
    return this.http.post<Anchor>(this.config.get().apiUrl +
      '/escrow/put/' + identity.name + EscrowService.methodAppendix(method), json);
  }

  verifyEscrow(method: string, identity: Identity, anchor: Anchor): Observable<VerificationResult> {
    return this.http.post<VerificationResult>(this.config.get().apiUrl + 
      '/escrow/verify/' + identity.name + EscrowService.methodAppendix(method), anchor);
  }

  getIdentityFromEscrow(anchor: Anchor): Observable<Identity> {
    return this.http.post<Identity>(this.config.get().apiUrl + 
      '/escrow/get', anchor);
  }

  getEscrowStatus(method: string, identity: Identity): Observable<EscrowStatus> {
    return this.http.get<EscrowStatus>(this.config.get().apiUrl + 
      '/escrow/status/' + identity.name + EscrowService.methodAppendix(method));
  }
}
