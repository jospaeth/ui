export class IdProvider {
    constructor(
        public url: string,
        public logoutURL: string,
        public name: string){}
}