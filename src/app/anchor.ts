export class Anchor {
    constructor(public method: string,
        public name: string,
        public anchorData: string) { }
}
